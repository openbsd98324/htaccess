# htaccess


## htpasswd files

https://www.redim.de/blog/passwortschutz-mit-htaccess-einrichten


## Installation 

````
$ dpkg -l | grep apache2
ii  apache2                            2.4.25-3+deb9u13                  armhf        Apache HTTP Server
ii  apache2-bin                        2.4.25-3+deb9u13                  armhf        Apache HTTP Server (modules and other binary files)
ii  apache2-data                       2.4.25-3+deb9u13                  all          Apache HTTP Server (common files)
ii  apache2-utils                      2.4.25-3+deb9u13                  armhf        Apache HTTP Server (utility programs for web servers)
ii  libapache2-mod-php                 1:7.0+49                          all          server-side, HTML-embedded scripting language (Apache 2 module) (default)
ii  libapache2-mod-php7.0              7.0.33-0+deb9u12                  armhf        server-side, HTML-embedded scripting language (Apache 2 module)
````

````
/var/www/html$ ls 
index.html
````




/var/www/html/abc/.htaccess
````
AuthType Basic
AuthName "Password Protected Area"
AuthUserFile /var/www/html/abc/.htpasswd
Require valid-user
````


/var/www/html/abc/.htpasswd

https://www.redim.de/blog/passwortschutz-mit-htaccess-einrichten










